import template from './listTodo.html';
import controller from './listTodo.controller';

export default {
    template,
    controller
};
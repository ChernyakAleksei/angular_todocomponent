import ng from 'angular';

import ListTodoComponent from './listTodo.component';

export default ng.module('app.components.listTodo', [])
    .component('listTodo', ListTodoComponent)
    .name;
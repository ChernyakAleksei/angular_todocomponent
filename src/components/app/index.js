import ng from 'angular';
import TodoService from './todos.service';

import AppComponent from './app.component';

export default ng.module('app.components.app', [])
    .value('Title', 'We love Angular')
    .component('app', AppComponent)
    .service('TodoService', TodoService)
    .name;
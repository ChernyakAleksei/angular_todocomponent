import template from './app.html';
import controller from './app.controller';

export default {
    bindings: {
        'showwidgets': '=',
    },
    template,
    controller,
};
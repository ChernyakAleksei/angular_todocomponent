export default class TodoService {

    getTodos() {
        return [{
            id: 0,
            title: 'Cras justo odio',
            isComplete: true
        }, {
            id: 1,
            title: 'Dapibus ac facilisis in',
            isComplete: false
        }, {
            id: 2,
            title: 'Morbi leo risus',
            isComplete: false
        }, {
            id: 3,
            title: 'Porta ac consectetur ac',
            isComplete: false
        }, {
            id: 4,
            title: 'Vestibulum at eros',
            isComplete: true
        }, ];
    }
}
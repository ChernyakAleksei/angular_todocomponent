export default class AppController {

    constructor(TodoService) {
        this.TodoService = TodoService;
        this.todos = this.TodoService.getTodos();
    }

    $onInit() {
        //this.title = Title;
        this.title = 'We love Angular';
        this.newTitle = '';
        this.isShow = true;
        this.todolen = this.todos.length;
    }
}

AppController.prototype.fixMe = function(e) {
    this.todos.title = 'ToDo List';
}

AppController.prototype.onBlur = function(e) {
    console.log(e);
}

AppController.prototype.addToDo = function(e) {
    e.preventDefault();
    this.todos.push({
        id: this.todolen++,
        title: this.newTitle,
        isComplete: false
    });
    this.newTitle = '';
    console.log(this.todolen);
}

AppController.prototype.remove = function(e, id) {
    e.preventDefault();
    const index = this.todos.findIndex(function(item) {
        return item.id === id;
    });
    this.todos.splice(index, 1);
}

AppController.prototype.save = function(e, todo) {
    delete todo.$edit;
}

AppController.prototype.focus = function(e) {
    e.currentTarget.parentElement.children[1].focus();
}
import ng from 'angular';
import ListTodo from './components/listTodo';
import App from './components/app';
import 'bootstrap/less/bootstrap.less';

ng.module('app', [ListTodo, App]);
//.value('Title', 'We love Angular');